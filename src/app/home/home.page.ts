import { Component } from '@angular/core';
import { Student } from './../model/student';
import {  PokerService } from './../providers/poker.service';
import { Router } from '@angular/router';
import { NavController} from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public studentList :any=[];
  public pokeData :any=[];
  public resultsActive : Array<Student> =[];
  public acum: any;
  public value: number;
  public data:boolean = false;


  constructor(   public navCtrl: NavController,private pokerService:PokerService,public router: Router ) {

  }

  ngOnInit() {
   
  }



  public show() {
    let i = Math.floor(Math.random() * 100);
    this.getApiData(`/${i}`);

  }



  
  /*      Get data                      */
  public getApiData(url:string):void{
    this.pokerService.get(url).subscribe(data=>{
      console.log('data');
      this.pokeData=data;
      this.data=true;
      console.log('valores devuelto de pokemon');
      console.log(this.pokeData);
      
    })

 }



}
