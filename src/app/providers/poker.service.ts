import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable,of} from  'rxjs';
import { catchError, map } from 'rxjs/operators';
import { URL } from '../providers/constants';

@Injectable({
  providedIn: 'root'
})
export class PokerService {

  constructor(private http: HttpClient) { }

  public get(route:string): Observable<any> {

    return this.http.get( URL.BASE+route).pipe(
        map(
          (resp: any) => {
            return resp;
            } 
          ),
        catchError(this.handleError<any>())
    );
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      return of(result as T);
  };
}
}
